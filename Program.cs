﻿using DnsClient;
using DnsClient.Protocol;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace DotnetDomainChecker
{
    // https://docs.microsoft.com/en-us/dotnet/api/system.iequatable-1.equals?view=net-6.0
    // https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.list-1?view=net-6.0
    // https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/quality-rules/ca1066
    // https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/quality-rules/ca1067
    // https://docs.microsoft.com/en-us/dotnet/standard/design-guidelines/choosing-between-class-and-struct
    // https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/struct
    // https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/how-to-override-the-tostring-method
    // https://docs.microsoft.com/en-us/dotnet/csharp/fundamentals/object-oriented/
    // https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/named-and-optional-arguments
    // public struct Domain : IEquatable<Domain>
    // public class Domain
    public struct Domain
    {
        public string Url { get; set; }
        public string Tld { get; set; }
        public string Record { get; set; }

        public Domain(string url, string tld, string record)
        {
            Url = url;
            Tld = tld;
            Record = record;
        }

        public override string ToString() => $"{Url}.{Tld}\t{Record}";
    }

    class Program
    {
        // static WebProxy proxy = new WebProxy
        private static readonly WebProxy proxy = new()
        {
            Address = new Uri(Environment.GetEnvironmentVariable("DOTNET_SocksProxy")),
        };
        /// <summary>
        /// Create an HttpClientHandler object and modify default proxy settings
        /// </summary>
        // static HttpClientHandler handler = new HttpClientHandler()
        private static readonly HttpClientHandler handler = new()
        {
            Proxy = proxy,
        };
        // HttpClient is intended to be instantiated once per application, rather than per-use. See Remarks.
        // private static readonly HttpClient client = new HttpClient(handler);
        static readonly HttpClient client = new(handler);

        // https://github.com/MichaCo/DnsClient.NET
        // https://dnsclient.michaco.net/documentation/readme
        // https://docs.microsoft.com/en-us/dotnet/fundamentals/code-analysis/style-rules/ide0090
        // https://dnsclient.michaco.net/docs/DnsClient.LookupClientOptions.html
        // https://dnsclient.michaco.net/docs/DnsClient.DnsQueryOptions.html#DnsClient_DnsQueryOptions_UseCache
        // https://blog.cloudflare.com/welcome-hidden-resolver/
        // https://dnsclient.michaco.net/docs/DnsClient.DnsQueryAndServerOptions.html#DnsClient_DnsQueryAndServerOptions_NameServers
        // https://github.com/MichaCo/DnsClient.NET/blob/master/src/DnsClient/NameServer.cs
        // https://1.1.1.1/
        // https://blog.cloudflare.com/welcome-hidden-resolver/
        // https://tor.cloudflare-dns.com/
        // https://www.cloudflare.com/learning/dns/what-is-1.1.1.1/
        // https://developers.cloudflare.com/1.1.1.1/
        // https://developers.cloudflare.com/1.1.1.1/other-ways-to-use-1.1.1.1/dns-over-tor
        // static readonly IPEndPoint endpoint = new(IPAddress.Parse("127.0.0.1"), 9080);
        // static readonly LookupClientOptions options = new(endpoint)
        static readonly LookupClientOptions options = new(NameServer.Cloudflare)
        {
            ThrowDnsErrors = true,
            UseCache = true,
        };
        static readonly LookupClient lookup = new(options);

        /// <summary>
        /// This method sets a default value to a not existing environment variable.
        /// <example>For example:
        /// <code>
        ///    SetDefaultEnv("DOTNET_SocksProxy", "socks5://localhost:9050");
        /// </code>
        /// </example>
        /// </summary>
        private static void SetDefaultEnv(string GetEnv, string SetEnv)
        {
            if (String.IsNullOrEmpty(Environment.GetEnvironmentVariable(GetEnv)))
            {
                Environment.SetEnvironmentVariable(GetEnv, SetEnv);
            }
        }

        // https://stackoverflow.com/questions/1952153/what-is-the-best-way-to-find-all-combinations-of-items-in-an-array
        // https://stackoverflow.com/questions/33312488/permutations-of-numbers-in-array
        static IEnumerable<IEnumerable<T>> GetPermutationsWithRept<T>(IEnumerable<T> list, int length)
        {
            if (length == 1) return list.Select(t => new T[] { t });
            return GetPermutationsWithRept(list, length - 1)
                .SelectMany(t => list,
                    (t1, t2) => t1.Concat(new T[] { t2 }));
        }

        /// <summary>
        /// Generates every possible character combination within a length.
        /// TLDs to watch: .hu, .com, .net, .org, .info, .xyz, .co
        /// HandshakeTLDs to watch: .p, .oo
        /// <example>For example:
        /// <code>
        ///    RandomStringGen("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 3, "zz0");
        /// </code>
        /// </example>
        /// <see href="https://ericlippert.com/2010/06/28/computing-a-cartesian-product-with-linq/">Computing a Cartesian product with LINQ</see>
        /// <see href="https://stackoverflow.com/questions/1952153/what-is-the-best-way-to-find-all-combinations-of-items-in-an-array">What is the best way to find all combinations of items in an array?</see>
        /// <see href="https://docs.microsoft.com/en-us/dotnet/api/system.string.compare?view=net-6.0#System_String_Compare_System_String_System_String_System_StringComparison_">Compare(String, String, StringComparison)</see>
        /// <see href="https://docs.microsoft.com/en-us/dotnet/api/system.stringcomparison?view=net-6.0">StringComparison Enum</see>
        /// <see href="https://docs.microsoft.com/en-us/dotnet/csharp/how-to/compare-strings">How to compare strings in C#</see>
        /// </summary>
        /// <param name="charset">A list of characters as a string.</param>
        /// <param name="length">A number for the length of characters.</param>
        /// <returns>A string array.</returns>
        private static List<string> RandomStringGen(string charset, int length, string startfrom = "0")
        {
            char[] characters = charset.ToLower().ToCharArray();
            Array.Sort(characters);
            // List<string> results = new List<string>();
            List<string> results = new();
            var enumerate = GetPermutationsWithRept(characters, length).ToList();

            foreach (IEnumerable<char> i in enumerate)
            {
                var str = string.Concat(i);
                // int compareLinguistic = String.Compare(str, startfrom, StringComparison.InvariantCultureIgnoreCase);
                int compareOrdinal = String.Compare(str, startfrom, StringComparison.OrdinalIgnoreCase);

                if (compareOrdinal > 0)
                    results.Add(str);
            }

            return results;
        }

        // https://docs.microsoft.com/en-us/dotnet/api/system.net.dns.gethostentry?view=net-6.0
        // https://docs.microsoft.com/en-us/dotnet/api/system.net.dns.gethostentryasync?view=net-6.0
        // https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/async/async-return-types
        private static async Task<bool> IfDomainExist(string hostname)
        {
            try
            {
                await Dns.GetHostEntryAsync(hostname);
                // await Dns.GetHostAddressesAsync(hostname);
            }
            catch (SocketException)
            {
                return false;
            }

            return true;
        }

        // https://github.com/MichaCo/DnsClient.NET/blob/master/src/DnsClient/DnsResponseCode.cs
        private static async Task DnsClientIfDomainExist(string hostname, string tld)
        {
            try
            {
                var result = await lookup.QueryAsync($"{hostname}.{tld}", QueryType.A);
                var record = result.Answers.ARecords().FirstOrDefault();

                /* var result = await lookup.QueryAsync($"{hostname}.{tld}", QueryType.SOA);
                var mname = result.Answers.SoaRecords().FirstOrDefault()?.MName; */

                /* var lResult = await lookup.QueryAsync($"{hostname}.{tld}", QueryType.A);
                var ip = lResult.Answers.OfType<ARecord>().FirstOrDefault()?.Address;
                domain = new(url: hostname, tld: tld, record: $"{ip}"); */
            }
            // catch (DnsResponseException e) when (e.Code.ToString() == "NotExistentDomain")
            catch (DnsResponseException e)
            {
                if (e.Code.ToString() == "NotExistentDomain")
                {
                    Domain domain = new(url: hostname, tld: tld, record: $"Error message: {e.Message}");
                    Console.WriteLine(domain);
                }
            }
        }

        static async Task Main(string[] args)
        {
            // SetDefaultEnv("http_proxy", "localhost:9080");
            // SetDefaultEnv("https_proxy", "localhost:9080");
            SetDefaultEnv("DOTNET_SocksProxy", "socks5://localhost:9050");
            Console.WriteLine("DOTNET_SocksProxy: {0}", Environment.GetEnvironmentVariable("DOTNET_SocksProxy"));

            // var domains = RandomStringGen("ABC", 2, "a0");
            // var domains = RandomStringGen("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 3, "000");
            // var domains = RandomStringGen("ABCDEFGHIJKLMNOPRSTUVXZ0123456789", 2, "00");
            var domains = RandomStringGen("ABCDEFGHIJKLMNOPRSTUVXZ0123456789", 3, "000");
            foreach (string i in domains)
            {
                await DnsClientIfDomainExist(i, "net");
                // await Task.Delay(1000);
            }
            Console.WriteLine();

            // Call asynchronous network methods in a try/catch block to handle exceptions.
            /* try
            {
                // https://docs.microsoft.com/en-us/dotnet/api/system.net.http.httpclient?view=net-6.0
                // https://docs.microsoft.com/en-us/azure/architecture/antipatterns/improper-instantiation/
                // https://stackoverflow.com/questions/4015324/how-to-make-an-http-post-web-request
                string responseBody = await client.GetStringAsync("https://ifconfig.me/ip");
                Console.WriteLine($"TOR: {responseBody}");
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0}", e.Message);
            } */
        }
    }
}
