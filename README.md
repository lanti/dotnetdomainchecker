# DotnetDomainChecker

## Usage

```sh
# https://docs.microsoft.com/en-us/ef/core/cli/dotnet
$ dotnet tool install --global dotnet-ef
$ dotnet tool update --global dotnet-ef
$ dotnet ef # Verify

# Initializing the dotnet project
$ dotnet new console
$ dotnet new gitignore

# VSCODE: ".NET: Generate Assets for Build and Debug"
$ dotnet run
# To build and test the Release version of your console application, open the Terminal and run the following command:
$ dotnet run --configuration Release
# Publish a .NET console application using Visual Studio Code
$ dotnet publish --configuration Release

# Add configuration to default console apps
# https://docs.microsoft.com/en-us/dotnet/core/extensions/configuration
$ dotnet add package Microsoft.Extensions.Hosting --version 6.0.0-rc.1.21451.13
$ dotnet add package DnsClient --version 1.5.0
$ dotnet add package morelinq --version 3.3.2
$ dotnet remove package morelinq
$ dotnet list package
# https://docs.microsoft.com/en-us/dotnet/core/extensions/configuration-providers#environment-variable-configuration-provider
# https://docs.microsoft.com/en-us/aspnet/core/fundamentals/startup?view=aspnetcore-5.0
# https://dotnetcoretutorials.com/2021/07/11/socks-proxy-support-in-net/
```
